package com.open_weather.model;


import lombok.Data;


@Data
public class YamlConfigurations {

    private String version;

    private String released;

    private String url;


}
