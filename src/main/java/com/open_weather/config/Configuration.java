package com.open_weather.config;


import com.open_weather.model.YamlConfigurations;
import lombok.extern.slf4j.Slf4j;
import org.yaml.snakeyaml.Yaml;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;


@Slf4j
public class Configuration {

    // Base URI
    private static final String URI;

    public static final String ENV = "env";


    private Configuration () {}


    static {
        try (
            InputStream inputStream = Files.newInputStream(
                Paths.get(
                    "src",
                    "main",
                    "resources",
                    "configuration-" + System.getProperty(ENV) + ".yaml"
                ))
        ) {
            YamlConfigurations configs = new Yaml().loadAs(inputStream, YamlConfigurations.class);
            URI = configs.getUrl();


        } catch (IOException e) {
            log.error(e.getMessage());
            throw new ExceptionInInitializerError(e);
        }
    }

    public static String getUri () {

        return URI;
    }
}
