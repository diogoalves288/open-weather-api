Feature: Get weather using Open Weather API

  Scenario: Validate that is possible to retrieve the weather by city name
    When the user requests the weather by city name "London"
    Then the user receives a response with status code 200
    And the response contains the weather information

  Scenario: Validate that is possible to retrieve the weather by geographic coordinates
    When the user requests the weather by latitude 35 and longitude 139
    Then the user receives a response with status code 200
    And the response contains the weather information
