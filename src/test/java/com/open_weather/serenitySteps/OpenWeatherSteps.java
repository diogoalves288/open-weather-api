package com.open_weather.serenitySteps;


import com.open_weather.rest.OpenWeatherEndpoints;
import io.restassured.response.Response;
import lombok.extern.slf4j.Slf4j;

import static com.open_weather.rest.CommonSpec.spec;


/**
 * This class contains all the steps and operations related with Open Weather
 */
@Slf4j
public class OpenWeatherSteps {

    /**
     * Method to retrieve the weather information by city name
     *
     * @param cityName name of the city to which the weather will be returned
     * @return the json response with the weather information
     */
    public static Response getWeatherByCityName (String cityName) {

        Response response = spec().get(
            OpenWeatherEndpoints.WEATHER_CITY_NAME_URI,
            cityName
        );

        log.info("Response: " + response.getBody().asString());

        return response;
    }


    /**
     * Method to retrieve the weather information by Geographic Coordinates
     *
     * @param latitude is the corresponding latitude of the geographical coordinates
     * @param longitude is the corresponding longitude of the geographical coordinates
     * @return the json response with the weather information
     */
    public Response getWeatherByGeographicCoordinates (int latitude, int longitude) {

        Response response = spec().get(
            OpenWeatherEndpoints.WEATHER_GEO_COORD_URI,
            latitude,
            longitude
        );

        log.info("Response: " + response.getBody().asString());

        return response;
    }
}
