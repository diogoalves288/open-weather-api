package com.open_weather.serenitySteps;


import io.restassured.response.Response;


public class HttpCommonSteps {

    public HttpCommonSteps () {

    }


    public Integer getStatusCode (Response response) {

        return response.getStatusCode();
    }

}

