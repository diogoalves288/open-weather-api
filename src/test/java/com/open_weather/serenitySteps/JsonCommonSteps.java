package com.open_weather.serenitySteps;


import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.Predicate;
import io.restassured.response.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;


public class JsonCommonSteps {

    private static final Logger log = LoggerFactory.getLogger(JsonCommonSteps.class);


    public JsonCommonSteps () {

    }


    public <T> T extractValue (Response response, String jsonPath) {

        T value = JsonPath.read(response.getBody().asString(), jsonPath, new Predicate[0]);
        log.info("Extracted from {} the value {}", jsonPath, value);
        return value;
    }


    public <T> T extractValue (Response response, String jsonPath, Object... jsonValues) {

        return jsonValues != null && jsonValues.length != 0 ? this.extractValue(
            response,
            String.format(jsonPath, jsonValues)
        ) : this.extractValue(response, jsonPath);
    }


    public Boolean extractValueAsBoolean (Response response, String jsonPath) {

        return (Boolean) this.extractValue(response, jsonPath);
    }


    public <T> List<T> extractValueAsList (Response response, String jsonPath) {

        return (List) this.extractValue(response, jsonPath);
    }


    public <T> List<T> extractValueAsList (Response response, String jsonPath, Object... jsonValues) {

        return (List) this.extractValue(response, jsonPath, jsonValues);
    }

}
