package com.open_weather.json;


public class OpenWeatherPaths {

    private OpenWeatherPaths () {}


    public final static String
        WEATHER_ID =
        "$.weather[*].id";

    public final static String
        WEATHER_MAIN =
        "$.weather[*].main";

    public final static String
        WEATHER_DESCRIPTION =
        "$.weather[*].description";

    public final static String
        WEATHER_ICON =
        "$.weather[*].icon";
}