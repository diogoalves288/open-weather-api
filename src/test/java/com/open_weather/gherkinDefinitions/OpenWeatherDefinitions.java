package com.open_weather.gherkinDefinitions;


import com.open_weather.json.OpenWeatherPaths;
import com.open_weather.runner.World;
import com.open_weather.serenitySteps.JsonCommonSteps;
import com.open_weather.serenitySteps.OpenWeatherSteps;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import lombok.extern.slf4j.Slf4j;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;


@Slf4j
public class OpenWeatherDefinitions {

    @Steps
    private JsonCommonSteps jsonSteps;

    @Steps
    private OpenWeatherSteps openWeatherSteps;


    @When("^the user requests the weather by city name \"([^\"]*)\"$")
    public void theUserRequestsTheWeatherByCityName (String cityName) {

        World.response = openWeatherSteps.getWeatherByCityName(cityName);
    }


    @Then("^the response contains the weather information$")
    public void theResponseContainsTheWeatherInformation () {

        Integer id = (Integer) jsonSteps.extractValueAsList(
            World.response,
            OpenWeatherPaths.WEATHER_ID
        ).get(0);

        Assert.assertNotNull("The weather id is empty", id);

        String main = (String) jsonSteps.extractValueAsList(
            World.response,
            OpenWeatherPaths.WEATHER_MAIN
        ).get(0);

        Assert.assertNotNull("The weather id is empty", main);

        String description = (String) jsonSteps.extractValueAsList(
            World.response,
            OpenWeatherPaths.WEATHER_DESCRIPTION
        ).get(0);

        Assert.assertNotNull("The weather id is empty", description);

        String icon = (String) jsonSteps.extractValueAsList(
            World.response,
            OpenWeatherPaths.WEATHER_ICON
        ).get(0);

        Assert.assertNotNull("The weather id is empty", icon);
    }


    @When("^the user requests the weather by latitude (\\d+) and longitude (\\d+)$")
    public void theUserRequestsTheWeatherByLatitudeAndLongitude (int latitude, int longitude) {

        World.response = openWeatherSteps.getWeatherByGeographicCoordinates(latitude, longitude);
    }
}
