package com.open_weather.gherkinDefinitions;


import com.open_weather.runner.World;
import com.open_weather.serenitySteps.HttpCommonSteps;
import cucumber.api.java.en.Then;
import lombok.extern.slf4j.Slf4j;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;

import static org.hamcrest.Matchers.is;


@Slf4j
public class RestDefinitions {

    @Steps
    private HttpCommonSteps httpSteps;


    @Then("^the user receives a response with status code (\\d+)$")
    public void validateResponseStatusCode (int statusCode) {

        Assert.assertThat(httpSteps.getStatusCode(World.response), is(statusCode));
    }


}
