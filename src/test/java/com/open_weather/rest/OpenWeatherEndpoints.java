package com.open_weather.rest;


public class OpenWeatherEndpoints {


    private OpenWeatherEndpoints () {}


    public static final String
        WEATHER_CITY_NAME_URI =
        "/weather?q={cityName}&appid=b6907d289e10d714a6e88b30761fae22";

    public static final String
        WEATHER_GEO_COORD_URI =
        "/weather?lat={latitude}&lon={longitude}&appid=b6907d289e10d714a6e88b30761fae22";

}
