package com.open_weather.rest;


import com.open_weather.config.Configuration;
import io.restassured.specification.RequestSpecification;

import static net.serenitybdd.rest.SerenityRest.rest;


public final class CommonSpec {

    public static RequestSpecification spec () {

        return rest()
            .baseUri(Configuration.getUri())
            .when();
    }

}