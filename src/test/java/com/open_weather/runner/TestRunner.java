package com.open_weather.runner;


import com.open_weather.config.Configuration;
import cucumber.api.CucumberOptions;
import lombok.extern.slf4j.Slf4j;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;


@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
    features = {"src/test/resources/features"},
    plugin = {"pretty", "html:target/cucumber", "json:target/cucumber-report.json"},
    glue = {"com.open_weather.gherkinDefinitions"}
)
@Slf4j
public class TestRunner {

    private TestRunner () {

    }


    @BeforeClass
    public static void setEnvironment () {

        String env = System.getProperty(Configuration.ENV);

        if (!env.equalsIgnoreCase("QA")) {
            log.error("Wrong value for " + Configuration.ENV + " property. The value as '" + env + "'");
            throw new ExceptionInInitializerError();
        }
        log.info("Running for Environment: " + env.toUpperCase());
    }

}
