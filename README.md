# Serenity BDD with Rest Assured
Originally cloned from Serenity BDD - BE Web seed project that is intended to fast track the creation and configuration of a **TAS** (Test Automation Solution) for **Back End Scenarios**.<br/>
This is a simple project to demo how to use Serenity BDD, Cucumber and RestAssured to test microservices.
It uses Gradle and Java 8.

## Getting Started

### Prerequisites

- Any software to clone this repository (e.g. SourceTree, Git installed locally...)
- Java 8 JDK

**NOTE:** Gradle local installation is not needed because is integrated in the project with the Wrapper.

### Installing

Clone this repository

## Running the tests

1.Use below command to execute the tests:

```
gradlew clean test aggregate -Denv=qa
```

2.If all works fine you will find an index.html file inside **target/site/serenity** folder with the results of the tests.


#### Scope
 - Serenity BDD (Cucumber based) framework schema
 - Serenity BDD, Cucumber and RestAssured to test microservices
 - Execution operations: GET, POST, PUT, DELETE
 - Validation of the content of the Reponse(body)
 - Set and validate Headers
 - Set Bodys request in different formats
 - Execute Collections


### Troubleshooting

- "On IntelliJ I can't go to the Step definitions of any step in a feature file, it doesn't detect the definition and suggests me to create one"
> Answer: Make sure you have the `Cucumber for Java` plugin installed and deactivate the `Substeps IntelliJ Plugin`. Restart IntelliJ and you should now be able to go to step definition of steps.

- "Running a single scenario from IntelliJ"
> Answer: To run a specific scenario from IntelliJ, for example faced to staging environment, don't forget to add `-Dproperties=serenity_stg.properties` to the `VM Options` to that scenario Run Configuration.

- "The annotations in the project are not working"
> Answer: Install `Lombok Plugin` and go to `File > Setting > Annotation Processors` and enable `Annotation Processing`
